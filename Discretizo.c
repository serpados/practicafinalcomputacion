#include <omp.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

//  Constantes 
#define CPU_TIME (clock_gettime( CLOCK_REALTIME, &ts), (double)ts.tv_sec +	\
		  (double)ts.tv_nsec * 1e-9)
#define tamaniovector 100      

// Variables
struct timeval *t0, *t1;
long i;
struct  timespec ts;

int valores_014;
int valores_1524;
int valores_25_64;
int valores_65;
int vectorResultados[4];
int nprocesadores=8;
long vector[tamaniovector];
long tamaniovectorArgumento;

/*
* Programa Principal
*/

int main (int argc, char * argv[])
{
    /* Paso de número de elementos del vector por argumento */
      if ( argc > 0 )
    tamaniovectorArgumento = atoi( *(argv+1) );
    
    long vector[tamaniovectorArgumento];

    // Declaración del número de hilos
    omp_set_num_threads(nprocesadores);
    double tstart=CPU_TIME;
    
    /* 1.Inicialización del Vector */

    srand((unsigned)time(0));

    #pragma omp parallel for 
    for(i=0; i<tamaniovectorArgumento; i++) {
        vector[i] = rand() % 96;
        //printf("array[%d] == %d\n", i, vector[i]);
    }

    /* 2. Ejecución  */

    int chunk=tamaniovectorArgumento/nprocesadores;

    #pragma omp parallel 
    {
    printf("Hilo número %d\n", omp_get_thread_num()); 

    #pragma omp for schedule(static,chunk) reduction(+:valores_014,valores_1524,valores_25_64,valores_65)
    for (i=0;i<tamaniovectorArgumento;i++)
    {
        if ((vector[i]>=0) && (vector[i]<15)) valores_014++;
        if ((vector[i]>=15) && (vector[i]<25)) valores_1524++;
        if ((vector[i]>=25) && (vector[i]<65)) valores_25_64++;
        if (vector[i]>=65) valores_65++;
    }
    
    #pragma omp critical
    {
    vectorResultados[0]=valores_014;
    vectorResultados[1]=valores_1524;
    vectorResultados[2]=valores_25_64;
    vectorResultados[3]=valores_65;
    }

    }

    /* 3. Resultados */
    
    printf("El número de personas entre 0 y 15 años es %d\n", vectorResultados[0]);
    printf("El número de personas entre 15 y 24 años es %d\n", vectorResultados[1]);
    printf("El número de personas entre 25 y 64 años es %d\n", vectorResultados[2]);
    printf("El número de personas mayores de 65 años es %d\n", vectorResultados[3]);
       
    double tend  = CPU_TIME;
    printf("El tiempo transcurrido en total ha sido %g unidades de tiempo\n", tend - tstart);
    return 0;
}
